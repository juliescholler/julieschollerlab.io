---
authors:
- admin
bio: essai
education: ""
email: ""
interests: ""
name: ""
organizations:
- name: Université de Tours
  url: "https://www.univ-tours.fr/site-de-l-universite/accueil--598731.kjsp"
role: Enseignante en Statistique et Mathématiques
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto: julie.scholler@univ-tours.fr'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/Pr_JScholler
- icon: cv
  icon_pack: ai
  link: "files/cv-julie_scholler.pdf"
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: github
#  icon_pack: fab
#  link: https://github.com/gcushen
superuser: true
user_groups:
- Researchers
- Visitors
---

## Mathématiques, Statistique et Analyse de données

### Liens directs 

* Licence d'économie [L1](/lecot/#lecot1) [L2](/lecot/#lecot2) [L3](/lecot/#lecot3)
* Master Économiste d'Entreprise [M1 et M2](#mecen)
