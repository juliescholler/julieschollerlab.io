+++
title = "L2 Mathématiques pour l'économiste 4 - 2020/2021"
date = 2021-01-12T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Suites récurrentes et équations différentielles"

summary = "Suites récurrentes et équations différentielles"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","maths","licence","L2","2021"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié", url = "L2ECO-CoursComplet-2021.pdf"},
{name = "Exercices", url = "L2ECO-Maths4-Fascicule-2021.pdf"},
{name = "CM-Diapos-Intro", url = "L2ECO-M4-CM-pstation.pdf"},
{name = "CM-Diapos-C0", url = "L2ECO-M4-CM-C0.pdf"},
{name = "CM-Diapos-C1v1", url = "L2ECO-M4-CM-C1.pdf"},
{name = "CM-Diapos-C2", url = "L2ECO-M4-CM-C2.pdf"},
{name = "CM-Diapos-C3v2", url = "L2ECO-M4-CM-C3.pdf"},
{name = "CM-Diapos-C4", url = "L2ECO-M4-CM-C4.pdf"},
{name = "Méthodes", url = "L2ECO-FichesMethodes-2021.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Ce cours a été donné au printemps 2021.

## Objectifs

L’objectif de cet enseignement est d’acquérir les techniques de résolution d’équations dont les inconnues ne sont plus
des nombres mais des suites ou des fonctions. Une attention particulière sera portée sur des situations faisant intervenir
des paramètres.

## Contenu

Modèles dynamiques utiles pour la modélisation d’évolution de
situation économique

* Suites récurrentes
  - linéaires à coefficients constants d’ordre 1
  - d’ordre 1 quelconques
  - linéaires à coefficients constants d’ordre 2
* Équations différentielles
  - linéaires à coefficients constants d’ordre 1
  - d’ordre 1 quelconques
  - linéaires à coefficients constants d’ordre 2
