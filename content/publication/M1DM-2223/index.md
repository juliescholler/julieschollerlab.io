+++
title = "M1 Data Mining - 2022"
date = 2022-09-18T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Classification supervisée"

summary = "Classification supervisée"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mecen","AD","master","2223"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Classification with R", target="_blank", url = "files/M1/2223/M1-Datamining-TP-2022-classification.html"},
{name = "Regression with R", target="_blank", url = "files/M1/2223/M1-Datamining-TP-regression-2022"},
{name = "Unbalanced Data with R", target="_blank", url = "files/M1/2223/M1-Datamining-TP-unbalanced_data-2022.html"},
{name = "CM Assessing Model Accuracy", target="_blank", url = "files/M1/2223/M1-CM-errors.html"},
{name = "CM Decision Trees", target="_blank", url = "files/M1/2223/M1-CM-decision_tree.html"},
{name = "CM Ensemble Methods - 1", target="_blank", url = "files/M1/2223/M1-CM-ensemble_method.html"},
{name = "CM Ensemble Methods - 2", target="_blank", url = "files/M1/2223/M1-CM-boosting.html"},
{name = "CM Study Protocol", target="_blank", url = "files/M1/2223/M1-CM-Study_protocol.html"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Cet enseignement est donné en 2022.

## Objectifs

L’objectif de cet enseignement est la découverte et la mise en pratique de différentes méthodes de classification supervisée (la régression logistique n'est pas abordée ici). La mise en pratique est faite avec R.

## Plan du cours

* Analyse factorielle discriminante (linéaire et quadratique)
* Courbe ROC
* Estimation de l’erreur de prédiction (validation croisée, par échantillons bootstrap)
* Arbres de décision
* Agrégation de modèles (bagging, random forest, boosting)
* Gestion de données manquantes

## Organisation

Je partage ce cours avec Franck Piller. Je suis responsable de la deuxième partie : Estimation de l'erreur de prédiction, arbres de décision, agrégation de modèles et gestion des données manquantes.

