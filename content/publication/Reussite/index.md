+++
title = "Réussite des étudiants"
date = 2019-02-20T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = [""]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["1"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Étude de la réussite des étudiants de la licence d'Économie de Tours"

summary = "Étude de la réussite des étudiants de la licence d'Économie de Tours"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","reussite"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Étude 2016-217", url = "Stats-1617.pdf"},
{name = "Étude 2017-2018", url = "Stats-1718.pdf"},
{name = "Étude 2018-2019", url = "Stats-1819.pdf"},
{name = "JPO - 2020",  target="_blank", url = "files/JPO-1920.html"},
{name = "Mise à niveau en Maths", url = "LECOT-Maths-nouveaux_entrants.pdf"},]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
