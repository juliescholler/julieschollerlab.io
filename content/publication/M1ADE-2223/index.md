+++
title = "M1 Analyse de données exploratoire - 2022/2023"
date = 2022-09-06T06:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Analyse factorielle et classification non supervisée"

summary = "Analyse factorielle et classification non supervisée"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mecen","AD","master","2223"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "ACM", url = "M1-AnaExpl-ACM.pdf"},
{name = "Questions_UN_2017", url = "questions_2017_imp.csv"},
{name = "Votes_UN_2017", url = "UN_votes_imp_2017.csv"},
{name = "TP html", target="_blank", url = "files/M1/M1ADE-TP-2223.html"}
]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Cet enseignement est donné à l'automne 2021.

## Objectifs

Cet enseignement est l’occasion de découvrir des méthodes avancées d’exploration de données multidimensionnelles :
analyse factorielle et classification non supervisée. Ces méthodes seront mise en pratique avec le logiciel R.

## Plan du cours

* Analyse en composantes principales
* Analyse factorielle des correspondances
* Analyse en composantes multiples
* Classification non supervisée : classification hiérarchique ascendante, méthodes des moyennes mobiles

## Organisation

Je partage ce cours avec Franck Piller. Je suis responsable de la deuxième partie : ACM et classification non supervisée.
