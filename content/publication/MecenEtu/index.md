+++
title = "Mecen - Production des étudiants"
date = 2020-01-12T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Projets, Shiny, R, Tutoriels"

summary = "Projets, Shiny, R, Tutoriels"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mecen","AD","master","R","1920"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [
    {name = "League of Legends dash board", target="_blank", url = "https://loldashboard.shinyapps.io/lolproject/"},
    {name = "US pollution", target="_blank", url = "https://us-pollution-2000-2016-pollutants.shinyapps.io/Big_data/"},
    {name = "Métier de la data", target="_blank", url = "http://datajob.netlify.app"},
{name = "World Happiness Report", target="_blank", url = "https://dev-reporting-app.shinyapps.io/app_test/"},
{name = "Covid Vaccination", target="_blank", url = "https://covidinfomecen.netlify.app/"},
{name = "Model Finder", target="_blank", url = "https://modelfinder.shinyapps.io/model_finder/"},
{name = "Statistique Décisionnelle - ressources", target = "_blank", url = "https://big-data-techniques-de-classement.netlify.app/"},
{name = "Spotify Playlists", target="_blank", url = "https://camillephilippe.shinyapps.io/recommandation-playlist-spotify/"},
{name = "Appli Purrfect Jobs", target="_blank", url = "https://thomasm-mecen.shinyapps.io/Purrfect/"}
]


# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Sur cette page sont regroupés des travaux réalisés par les étudiants du Master Mécen.

## Projets de M2 - promo 2021/2022

- **Mélissa Billon** et **Jérémy Feteira** : dashboard Shiny sur la pollution de l'air aux États-Unis https://us-pollution-2000-2016-pollutants.shinyapps.io/Big_data/
- **Allan Guichard** et **Rémi Lefafta** : dashboard Shiny autour de League of Legends https://loldashboard.shinyapps.io/lolproject/

## Projets de M2 - promo 2020/2021

- **Aline Léger** et **François Boussengui** : site web présentant leur analyse d'offres d'emploi autour des métiers de l'analyse de donnée http://datajob.netlify.app
- **Mélissa Zennaf** et **Yasmine Akdag** : tableau de board Shiny explorant les entreprises tourangelles grâce à l'Opendata de Tours https://projetsbigdata.shinyapps.io/cartographie/
- **Gwenaëlle Nicoleau** et **Mohamad ElKawas** : tableau de bord Shiny explorant le bonheur dans le monde avec de jolis graphiques interactifs (et de jolies couleurs) https://dev-reporting-app.shinyapps.io/app_test/
- **Blandine Desroches** et **Nicolas Barré** : site web présentant des informations autour de la vaccination contre le covid (plein de graphique interactifs) https://covidinfomecen.netlify.app/
- **Théo De Sa Morais** et **Albane Sanchez** application Shiny permettant d'explorer et de choisir un type de modèle pour analyser des données https://modelfinder.shinyapps.io/model_finder/
- **Lilian Boissé**  et **Jean Clark** : site web regroupant des ressources autour de la statistique décisionnelle https://big-data-techniques-de-classement.netlify.app/
- **Camille Philippe** et **Grégoire Amato** : application Shiny permettant d'explorer les playlist Spotify selon ses envies du moment https://camillephilippe.shinyapps.io/recommandation-playlist-spotify/
- **Fried Sabaye** et **Caleb Kashala** : application Shiny pour explorer le réseau de transport transilien https://apptransilienmecen.shinyapps.io/apptransilien/
- **Loïck Rigardie** et **Antoine Thomas** : tableau de bord Shiny autour des paris sportifs (données non actualisées) https://rigardie-thomas-bigdata.shinyapps.io/app-paris/

## Projets de M2 - promo 2019/2020

- **Pauline Journoux** et **Margaux Thuard** vous proposent un [tutoriel](files/M2-etu/Journoux-Thuard-Cartographie_avec_R.html) pour faire de la cartographie avec R : cartes statiques, dynamiques, présenté via RMarkdown.

- **Bénicia Ekuba** et **Valentin Bonneau** vous présentent une [introduction](files/M2-etu/Bonneau-Ekuba-Notice_PowerBI.html) à PowerBI, PowerBI Desktop et son interfaçage avec R.

- **Juliette Thiret** et **Thomas Maurice** ont construit une application Shiny intitulée [Purrfect Jobs](https://thomasm-mecen.shinyapps.io/Purrfect/) destinée aux personnes en recherche active d'emploi permettant d'explorer une base de données d'emploi, les particularités de certaines villes et des informations sur des entreprises.   
Tout cela est accompagné d'une [présentation](/files/M2-etu/Thiret-Maurice-notice.html) du contenu de l'appli, détaillant comment ils ont affiné la présentation de l'appli notamment avec un style css, ainsi que d'une présentation de la méthode *t-SNE* utilisée en complément d'une méthode de classification non supervisée pour représenter les typologies d'offres d'emploi.

En bonus un [diaporama](/files/M2-etu/Thiret-Maurice-diapos.pdf) présentant plus en détails et images la méthode *t-SNE*.

- **Evelore Leclercq** et **Andréa Vieira** ont construit un bel exemple de  tableau de bord avec Shiny pour un hôtel.
Elle en ont également profiter pour partager [quelques astuces](/files/M2-etu/Leclercq-Vieira-Tuto_shiny.html) pour la création d'applications Shiny dont comment rendre une InfoBox cliquable.   
Comme elles ont utilisée *tidymodels* pour leur application, vous avez le droit en bonus à une [notice d'utilisation de tidymodels](/files/M2-etu/Leclercq-Vieira-Tuto_tidymodels.html).

- **Jordan Avrillon**, **Timothy Hervier** et **Lucas Pires** ont construit une application Shiny pour faciliter le tri parmi les différents algorithmes et méthodes de prédictions.   
Comme eux aussi ont utilisé *tidymodels*, ils vous offrent une [présentation beamer](/files/M2-etu/Avrillon-Hervier-Pires-Beamer-Tidymodels.pdf) et un [tutoriel](/files/M2-etu/Avrillon-Hervier-Pires-Beamer-Tuto-Tidymodelsl.html).



À suivre...
