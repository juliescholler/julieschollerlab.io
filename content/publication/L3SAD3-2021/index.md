+++
title = "L3 Statistiques et Analyse de Données 3 - 2020/21"
date = 2021-01-28T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Statistique bayésienne"

summary = "Statistique bayésienne"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","stats","licence","L3","2021"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Diapos-C1", url = "L3ECO-SD3-C1-etu.pdf"},
{name = "Diapos-C2", url = "L3ECO-SD3-C2-etu.pdf"},
{name = "Diapos-C3-v0", url = "L3ECO-SD3-C3-etu.pdf"},
{name = "Diapos-C4", url = "L3ECO-SD3-C4-etu.pdf"},
{name = "Exercices 1", url = "L3ECO-SAD3-TD1-2021.pdf"},
{name = "Exercices 2", url = "L3ECO-SAD3-TD2-2021.pdf"},
{name = "Exercices 3", url = "L3ECO-SAD3-TD3-2021.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Cet enseignement a été donné au printemps 2021.

## Objectifs

## Liens et ressources

- polycopié en anglais d'Alicia A. Johnson, Miles Ott et Mine Dogucu : [Bayes Rules!](https://www.bayesrulesbook.com/)
- playlist de vidéos autour de la statistique fréquentiste et des *p-value* : [lien](https://www.youtube.com/watch?v=xVIt51ybvu0&list=PLjD7j1kR73YTdQT6y8AqBtnbdJq-PiX_b)
- playlist de vidéos autour de la statistique et la pensée bayésiennes : [lien](https://www.youtube.com/watch?v=x-2uVNze56s&list=PLjD7j1kR73YTdlTNNI5WkNf8daf3byw-f)

## Plan du cours

* Regard critique sur la statistique fréquentiste
* Estimation bayésienne : introduction, estimation en utilisant des familles de lois conjuguée
* Tests bayésiens
