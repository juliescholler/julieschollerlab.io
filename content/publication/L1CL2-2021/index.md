+++
title = "L2 Calcul & Logique 2 - 2020/2021"
date = 2021-01-13T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Ensembles, dénombrement, nombres complexes, suites numériques"

summary = "Ensembles, dénombrement, nombres complexes, suites numériques"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","maths","licence","L1","2021"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié", url = "L1-CL2-Poly.pdf"},
{name = "Exercices", url = "L1-CL2-fasc-exos-etu.pdf"},
{name = "CM-C1-Ensembles", url = "L1ECOetM3-S1-CL2-C1.pdf"},
{name = "CM-C2-Complexes", url = "L1ECOetM3-S1-CL2-C2.pdf"},
{name = "CM-C3-Suites-v2302", url = "L1ECOetM3-S1-CL2-C3.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Ce cours a été donné au printemps 2021.

## Objectifs
L’objectif de cet enseignement est de travailler la rigueur dans les raisonnements et les calculs afin de maîtriser les techniques de base pour résoudre les problèmes qui seront rencontrés en économie.

## Contenu

* Ensemble et dénombrement
* Nombres complexes : solutions d’équations de degré 2, forme algébrique, forme exponentielle, interprétation géométrique
* Suites numériques : arithmétiques, géométriques, sommes de termes, limites

## Prérequis

* Calcul & Logique 1
* et/ou de bonnes bases en maths du lycée
