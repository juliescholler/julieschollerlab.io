+++
title = "L2 Statistiques et Analyse de Données 1 - 2021/2022"
date = 2021-09-03T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Échantillonnage et estimation"

summary = "Échantillonnage et estimation"

# Caption (optional)
caption = "Photo by [Academic](https://sourcethemes.com/academic/)"

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","stats","licence","L2","2122"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié", url = "L2-SAD1-poly-2122.pdf"},
{name = "Exercices", url = "L2-SAD1-fasc-exosETUDIANT-2122.pdf"},
{name = "Présentation", url = "L2-SAD1-CM-presentation-etu.pdf"},
{name = "CM-Thème 1", url = "L2-SAD1-CM-C1echantillonnage-etu.pdf"},
{name = "CM-Thème 2", url = "L2ECO-S1-CM-2estimation1-etuv2.pdf"},
{name = "CM-Thème 3", url = "L2ECO-S1-CM-3estimation2-etu.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Cet enseignement a été donné à l'automne 2021.


## Objectifs

L'objectif de cet enseignement est d'approfondir la maîtrise des outils probabilistes utiles à la compréhension et à la bonne utilisation des méthodes de statistique inférentielle, et d'introduire la théorie de l'estimation. Les principaux concepts de statistique inférentielle seront utilisés pour estimer les valeurs des paramètres d'une population, sur la base de résultats d'échantillon.

Ce sera l'occasion de favoriser l'analyse « critique » des données chiffrées issues des probabilités et de la statistique inférentielle.


## Pré requis

* vocabulaire de statistique descriptive : population, individu, types de variables (quantitative ou qualitative)
* manipulation de l'espérance et de la variance (très important)
* connaissance des lois classiques : Bernoulli, binomiale, normale

## Plan du cours

- Échantillonnage : échantillon, statistique, moyenne empirique, variance empirique, statistiques d'ordre, loi des grands nombres, théorème central limite ;
- Estimation de caractéristiques d'une loi : estimateurs, biais, erreur quadratique moyenne, intervalles de confiance d'espérances, de variances et de proportions ;
- Construction et choix d'estimateur : comparaison d'estimateur, méthode des moments, méthode du maximum de vraisemblance, intervalle de confiance.
