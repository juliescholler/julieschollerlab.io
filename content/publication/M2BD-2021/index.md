+++
title = "M2 Big Data - 2020/21"
date = 2021-01-10T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Tidyverse, Calcul parallèle, Réseaux de neurones, Projets"

summary = "Tidyverse, Calcul parallèle, Réseaux de neurones, Projets"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mecen","AD","master","R","2021"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Worflow in Data Analysis 1", target="_blank", url = "files/M2/2021/M2-CM1-workflow1.html"},
{name = "Worflow in Data Analysis 2", target="_blank", url = "files/M2/2021/M2-CM2-wfdawR.html"},
{name = "Worflow in Data Analysis 3", target="_blank", url = "files/M2/2021/M2-CM3-wfdawR.html"},
{name = "Worflow in Data Analysis 4", target="_blank", url = "files/M2/2021/M2-CM4-wfdawR.html"},
{name = "Ressources et exos", target="_blank", url = "files/M2/2021/ressources_et_exos.html"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Cet enseignement est donné au printemps 2021.

## Objectifs

Ce cours est l'occasion d'aborder divers thèmes théoriques ou techniques selon les demandes et les besoins.

## Plan du cours

Lien vers les ressources (base de données, scripts, etc.) : https://utbox.univ-tours.fr/s/3PTMmQdRQdCi9gM

En 2020-2021

Julie Scholler

* R : Tidyverse, applications Shiny
* High Performance Computing : quelques éléments sous R, survol d'autres solutions (Hadoop, Spark)
* Réseaux de neurones

Franck Piller

* Support Vector Machine
* Analyse textuelle

Projets étudiants

* ...
