+++
title = "L3 Logiciel R 3 - 2022/23"
date = 2022-09-10T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "R Markdown"

summary = "Production de document avec R Markdown"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","r","licence","L3","2223"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "CM-Diapos", target="_blank", url = "files/R3/2223/L3ECO-R3-CM.html"},
{name = "Sujet de TP", target="_blank", url = "files/R3/2223/L3ECO-R3-TP-2223-etu.html"},
{name = "BDD compact", url = "apprendre_a_vous_connaitre__rentree_2022-compact.csv"},
{name = "BDD séparé", url = "apprendre_a_vous_connaitre__rentree_2022-disjonction.csv"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "featured.png"
caption = ""

+++

Cet enseignement est donné à l'automne 2022.

## Ressources externes

- [Cheatsheet R Markdown](https://raw.githubusercontent.com/rstudio/cheatsheets/main/rmarkdown.pdf)
- [Fiche référence R Markdown](https://rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf)

## Objectifs

Le but de cet enseignement est de travailler la production de documents d’analyse statistique via le logiciel R. On utilisera le package RMarkdown et le langage Markdown pour la production de documents HTML et de documents pdf simples.

## Déroulement

L'enseignement se déroule en deux parties.

* Découverte de langage R Markdown, maîtrise de la mise en page du document produit
* Réalisation de document d'analyse d'une base de données sous forme de projet
