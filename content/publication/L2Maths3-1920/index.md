+++
title = "L2 Mathématiques pour l'économiste 3 - 2019/20"
date = 2019-09-12T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Algèbre linéaire"

summary = "Algèbre linéaire"

# Caption (optional)
caption = "Photo by [Academic](https://sourcethemes.com/academic/)"

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","maths","licence","L2","1920"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié v28/11", url = "L2-Maths3-poly-C1-C5.pdf"},
{name = "Diaporama C1", url = "L2ECO-maths3-CM-C1-ETU.pdf"},
{name = "Diaporama C2", url = "L2ECO-maths3-CM-C2-ETU-v2.pdf"},
{name = "Diaporama C3", url = "L2ECO-maths3-CM-C3-ETU-v1.pdf"},
{name = "Diaporama C4", url = "L2ECO-maths3-CM-C4-ETU-v2.pdf"},
{name = "Diaporama C5", url = "L2ECO-maths3-CM-C5-ETU-v1.pdf"},
{name = "Fascicule d'exercices", url = "L2ECO-Maths3-TD-ETU.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

Cet enseignement est donné à l'automne 2019.

## Objectifs

L'objectif de cet enseignement est d'acquérir les bases de l'algèbre linéaire afin de résoudre des problèmes (linéaires).


## Plan du cours

- Systèmes linéaires
- Matrices
- Espaces vectoriels
- Déterminants
- Diagonalisation de matrices et applications
