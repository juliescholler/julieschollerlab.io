+++
title = "L2 Logiciel R 1 - 2021/2022"
date = 2021-09-04T03:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["4"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Introduction au langage R"

summary = "Introduction au langage R"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","r","licence","L2","2122"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [
{name = "CM1-Diapos", target="_blank", url = "files/R1/L2ECO-R1-CM1-2122.html"},
{name = "Notes de cours - html", target="_blank", url = "files/R1/L2-R1-notedecours-2122.html"},
{name = "Sujets de TP - html", target="_blank", url = "files/R1/L2-R1-TP-sujets-2122.html"},
{name = "Ressources", url = "files/R1/L2-R1-sources-2122.zip"},
{name = "Notes de cours et TP - pdf", url = "L2ECO-R1-tout-2122.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = "featured.png"
caption = ""

+++

Cet enseignement a été donné à l'automne 2021.

## Objectifs
Le but de cet enseignement est de se familiariser avec le logiciel R afin d’apprendre à utiliser les outils de gestion de données, les outils statistiques et graphiques de base.

## Contenu

* Découverte du langage R : interface R Studio, manipulations et objets de base
* Études descriptives de données enregistrées dans un data frame
* Calcul matriciel, tableau de contingence
* Création et exportation de représentations graphiques
* Importation, exportation et manipulation de données
