+++
title = "L1 Statistiques Descriptives - 2022/2023"
date = 2022-08-10T01:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: administratif
#  2:  
#  3: divers mecen
#  4: archives
#  5: enseignements en cours
#  6: Tutoriel...
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Statistiques descriptives univariées et bivariées"

summary = "Statistiques descriptives univariées et bivariées"

# Caption (optional)
caption = ""

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"

# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","stats","licence","L1","2223"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié", url = "L1-SD-Poly.pdf"},
{name = "Diapos intro", url = "L1ECO-S1-SD-CM-pstation.pdf"},
{name = "Diapos CM1", url = "L1ECO-S1-SD-CM1-site.pdf"},
{name = "Exercices", url = "L1-StatDesc-fasc-exos.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

# share
share = false
+++

Ce cours est donné à l'automne 2022.

## Objectifs

* Savoir réaliser et comprendre les tableaux d'effectifs, de
fréquences, de contingence, les graphiques courants et les
données synthétiques utilisés pour résumer des données
* Développer l’esprit critique vis-à-vis des données numériques

## Contenu de l’enseignement
* Statistiques descriptives unidimensionnelles
tableaux synthétiques, graphiques, indicateurs de tendance
centrale, de dispersion, courbe de concentration, indice de Gini
* Statistiques descriptives bidimensionnelles
tableaux de contingence, liens entre variables, variances
expliquées et résiduelles, régression linéaire simple

