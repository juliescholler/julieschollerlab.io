+++
widget = "en-tete"  # The name of the widget that you created.
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 11  # Order that this section will appear in.


title = "Mathématiques, Statistique et Analyse de données"
subtitle = ""
author="admin2"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DeepSkyBlue"
  # gradient_end = "SkyBlue"
  
  # Background image.
  image = "headers/texture-b.png"  # Name of image in `static/img/`.
  image_darken = 0  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

### Liens directs 

* Licence d'économie [L1](/lecot/#lecot1) [L2](/lecot/#lecot2) [L3](/lecot/#lecot3)
* Master Économiste d'Entreprise [M1 et M2](#mecen)


