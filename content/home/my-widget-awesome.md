+++
widget = "about"  # The name of the widget that you created.
headless = true  # This file represents a page section.
active = false  # Activate this widget? true/false
weight = 12  # Order that this section will appear in.

author="admin2"

+++

### Liens directs 

* Licence d'économie [L1](/lecot/#lecot1) [L2](/lecot/#lecot2) [L3](/lecot/#lecot3)
* Master Économiste d'Entreprise [M1 et M2](#mecen)


